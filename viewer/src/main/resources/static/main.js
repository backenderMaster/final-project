function convertMetricToTableRow(metric) {
    return `<tr>
               <td>${metric.name}</td>
               <td>${metric.currentValue}</td>
               <td>${metric.borderValue}</td>
               <td>${metric.instrumentId}</td>
        </tr>`
}

function renderTable(metrics) {
    const tableBodyId = "tableBody"
    const tableBody = document.getElementById(tableBodyId)
	const rows = metrics.map(convertMetricToTableRow)
    tableBody.innerHTML = rows.join("")
}

function renderSnapshotTime(time) {
    const elementId = "snapshotTime"
    const element = document.getElementById(elementId)
    element.innerText = "Snapshot time: " + time.replace('T', ' ')
}

function renderSnapshot(snapshot) {
    renderTable(snapshot.metrics)
    renderSnapshotTime(snapshot.timestamp)
}

function renderMetrics() {
    const timeoutMillis = 2000
    setInterval(() => axios.get('/snapshot').then(response => renderSnapshot(response.data)), timeoutMillis)
}

renderMetrics()