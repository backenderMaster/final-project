insert into users (id, name, password, desk) values
    (2001, 'user1', '{bcrypt}$2a$10$CYnenekTU0UdceB63SyLDe8yRnk0HmpycPdQaVJYfe.5J54jt1Bz.', 'DESK_ONE'),
    (2002, 'user2', '{bcrypt}$2a$10$w6EhQM2jDsK4HT4pityLzOXua7PieTn6iR6dmTzwokfikBsdBsKBe', 'DESK_TWO');