package ru.otus.viewer.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.WebClient;

@Configuration
public class Config {
    @Bean
    public WebClient webClient(@Value("${calculator.host}") String host, @Value("${calculator.port}") int port) {
        String baseUrl = "http://"+ host + ":" + port + "/";
        return WebClient.builder().baseUrl(baseUrl).build();
    }
}
