package ru.otus.viewer.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import ru.otus.model.finance.Desk;
import ru.otus.model.metric.Snapshot;

@Component
public class SnapshotService {
    private final WebClient calculatorClient;
    private final String snapshotEndpoint;

    public SnapshotService(
        @Autowired WebClient calculatorClient,
        @Value("${calculator.snapshot.endpoint}") String snapshotEndpoint
    ) {
        this.calculatorClient = calculatorClient;
        this.snapshotEndpoint = snapshotEndpoint;
    }

    public Snapshot loadSnapshot(Desk desk) {
        return calculatorClient.get()
            .uri(snapshotEndpoint)
            .header("desk", desk.toString())
            .retrieve()
            .bodyToMono(Snapshot.class)
            .block();
    }
}
