package ru.otus.viewer.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import ru.otus.model.finance.Desk;
import java.util.List;

@Component
public class DeskProvider {
    private final DeskToAuthorityConverter converter;

    public DeskProvider(@Autowired DeskToAuthorityConverter converter) {
        this.converter = converter;
    }

    public Desk getDesk() {
        List<GrantedAuthority> list = List.copyOf(
            SecurityContextHolder.getContext().getAuthentication().getAuthorities()
        );
        int firstElementIndex = 0;
        GrantedAuthority grantedAuthority = list.get(firstElementIndex);
        return converter.convertToDesk(grantedAuthority);
    }
}
