package ru.otus.viewer.security;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;
import ru.otus.model.finance.Desk;

@Component
public class DeskToAuthorityConverter {
    private final String authorityPrefix = "VIEW_";

    public GrantedAuthority convertToAuthority(Desk desk) {
        return () -> authorityPrefix + desk.toString();
    }

    public Desk convertToDesk(GrantedAuthority authority) {
        return Desk.valueOf(authority.getAuthority().replace(authorityPrefix, ""));
    }
}