package ru.otus.viewer.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.security.core.userdetails.UserDetailsService;
import ru.otus.viewer.repository.UserRepository;
import org.springframework.security.core.userdetails.UserDetails;

@Service
public class UserLoader implements UserDetailsService {
    private final UserRepository repository;
    private final DeskToAuthorityConverter converter;

    public UserLoader(
        @Autowired UserRepository repository,
        @Autowired DeskToAuthorityConverter converter
    ) {
        this.repository = repository;
        this.converter = converter;
    }

    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        AppUser user;
        try {
            user = repository.findByName(username);
        } catch(Exception e) {
            throw new UsernameNotFoundException(e.getMessage());
        }

        return User.withUsername(user.getName())
            .password(user.getPassword())
            .authorities(converter.convertToAuthority(user.getDesk()))
            .build();
    }
}