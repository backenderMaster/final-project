package ru.otus.viewer.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.otus.viewer.security.AppUser;

public interface UserRepository extends JpaRepository<AppUser, Long> {
    AppUser findByName(String name);
}
