package ru.otus.viewer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ViewerApp {
    public static void main(String[] args) {
        SpringApplication.run(ViewerApp.class);
    }
}

