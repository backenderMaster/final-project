package ru.otus.viewer.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class PageController {
    @GetMapping("/api/metrics")
    public String getMetricPage() {
        return "metrics";
    }
}
