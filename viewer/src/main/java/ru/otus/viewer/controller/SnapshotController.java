package ru.otus.viewer.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.otus.model.finance.Desk;
import ru.otus.model.metric.Snapshot;
import ru.otus.viewer.security.DeskProvider;
import ru.otus.viewer.services.SnapshotService;

@RestController
public class SnapshotController {
    private final SnapshotService snapshotService;
    private final DeskProvider deskProvider;

    public SnapshotController(
        @Autowired SnapshotService snapshotService,
        @Autowired DeskProvider deskProvider
    ) {
        this.snapshotService = snapshotService;
        this.deskProvider = deskProvider;
    }

    @GetMapping("/snapshot")
    public Snapshot getSnapshot() {
        Desk desk = deskProvider.getDesk();
        return snapshotService.loadSnapshot(desk);
    }
}