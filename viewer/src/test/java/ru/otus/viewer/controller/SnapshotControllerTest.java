package ru.otus.viewer.controller;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import ru.otus.model.finance.Desk;
import ru.otus.model.metric.Snapshot;
import ru.otus.viewer.security.DeskProvider;
import ru.otus.viewer.services.SnapshotService;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

@WebMvcTest(SnapshotController.class)
class SnapshotControllerTest {
    @Autowired
    private MockMvc mvc;
    @MockBean
    private SnapshotService snapshotService;
    @MockBean
    private DeskProvider deskProvider;

    @WithMockUser(username = "user1", authorities = {"VIEW_DESK_ONE"})
    @ParameterizedTest
    @EnumSource(Desk.class)
    public void endpointMustLoadMetricsRelatedToAppropriateDesk(Desk desk) throws Exception {
        given(deskProvider.getDesk()).willReturn(desk);
        given(snapshotService.loadSnapshot(any())).willReturn(new Snapshot());
        mvc.perform(get("/snapshot"));
        verify(snapshotService).loadSnapshot(desk);
    }
}