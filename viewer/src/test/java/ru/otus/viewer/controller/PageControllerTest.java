package ru.otus.viewer.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.context.annotation.Import;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import ru.otus.viewer.security.SecurityConfiguration;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(PageController.class)
@Import(SecurityConfiguration.class)
class PageControllerTest {
    @Autowired
    private MockMvc mvc;
    private final String metricsPageUrl = "/api/metrics";

    @Test
    public void nonAuthenticatedUsersMustBeRedirectedToMainPage() throws Exception {
        String loginUrl = "http://localhost/login";
        mvc.perform(get(metricsPageUrl))
            .andExpect(status().is3xxRedirection())
            .andExpect(redirectedUrl(loginUrl));
    }

    @Test
    @WithMockUser(username = "user1", authorities = {"VIEW_DESK_ONE"})
    public void metricsPageMustBeAvailableForAuthenticatedUsers() throws Exception {
        mvc.perform(get(metricsPageUrl)).andExpect(status().isOk());
    }
}