package ru.otus.calculator.repositories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.mongodb.core.ReactiveMongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;
import ru.otus.model.metric.Snapshot;
import java.time.LocalDateTime;
import java.util.Comparator;

@Component
public class SnapshotRepository {
    private final ReactiveMongoOperations mongoTemplate;
    private final String collectionName;

    public SnapshotRepository(
        @Autowired ReactiveMongoOperations mongoTemplate,
        @Value("${snapshots.collection.name}") String collectionName
    ) {
        this.mongoTemplate = mongoTemplate;
        this.collectionName = collectionName;
    }

    public Mono<Snapshot> findLatestSnapshot() {
        int depthMinutes = 5;
        Query query = new Query(Criteria.where("timestamp").gte(LocalDateTime.now().minusMinutes(depthMinutes)));
        return mongoTemplate.find(query, Snapshot.class)
            .collectList()
            .filter(list -> !list.isEmpty())
            .map(snapshots -> {
                snapshots.sort(Comparator.comparing(Snapshot::getTimestamp));
                return snapshots.get(snapshots.size() - 1);
            });
    }

    public Mono<Snapshot> saveSnapshot(Snapshot snapshot) {
        return mongoTemplate.save(snapshot);
    }
}
