package ru.otus.calculator.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;
import ru.otus.calculator.repositories.SnapshotRepository;
import ru.otus.model.metric.Metric;
import ru.otus.model.metric.Snapshot;
import ru.otus.model.finance.Desk;
import java.util.Collection;
import java.util.stream.Collectors;

@RestController
public class SnapshotController {
    private final SnapshotRepository repository;

    public SnapshotController(@Autowired SnapshotRepository repository) {
        this.repository = repository;
    }

    @GetMapping("/snapshot")
    public Snapshot getSnapshot(@RequestHeader("desk") Desk desk) {
        Snapshot latestSnapshot = repository.findLatestSnapshot().block();
        Collection<Metric> metrics = latestSnapshot.getMetrics()
            .stream()
            .filter(metric -> metric.getDesk().equals(desk))
            .collect(Collectors.toList());
        return new Snapshot(latestSnapshot.getTimestamp(), metrics);
    }
}