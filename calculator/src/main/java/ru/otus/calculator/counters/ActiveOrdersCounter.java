package ru.otus.calculator.counters;

import ru.otus.model.finance.Execution;
import ru.otus.model.finance.ExecutionStatus;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

public class ActiveOrdersCounter implements Counter {
    private final ConcurrentHashMap<Long, ExecutionStatus> activeOrders;
    private final String instrumentId;

    public ActiveOrdersCounter(String instrumentId) {
        this.activeOrders = new ConcurrentHashMap<>();
        this.instrumentId = instrumentId;
    }

    @Override
    public void acceptExecution(Execution execution) {
        if (Objects.equals(instrumentId, execution.getInstrumentId())) {
            updateActiveOrdersMap(execution);
        }
    }

    private void updateActiveOrdersMap(Execution execution) {
        if (execution.getStatus() == ExecutionStatus.Initial || execution.getStatus() == ExecutionStatus.Intermediate) {
            activeOrders.put(execution.getOrderId(), execution.getStatus());
        }
        if (execution.getStatus() == ExecutionStatus.Terminal && activeOrders.get(execution.getOrderId()) != null) {
            activeOrders.remove(execution.getOrderId());
        }
    }

    @Override
    public long getValue() {
        return activeOrders.size();
    }
}