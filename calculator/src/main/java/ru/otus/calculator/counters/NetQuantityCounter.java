package ru.otus.calculator.counters;

import ru.otus.model.finance.Execution;

import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

public class NetQuantityCounter implements Counter {
    private final String instrumentId;
    private final AtomicInteger value;

    public NetQuantityCounter(String instrumentId) {
        this.instrumentId = instrumentId;
        value = new AtomicInteger(0);
    }

    @Override
    public void acceptExecution(Execution execution) {
        if (Objects.equals(execution.getInstrumentId(), instrumentId)) {
            value.addAndGet(execution.getQuantity());
        }
    }

    @Override
    public long getValue() {
        return value.get();
    }
}