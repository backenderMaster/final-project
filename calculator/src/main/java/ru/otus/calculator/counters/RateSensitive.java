package ru.otus.calculator.counters;

import ru.otus.model.finance.Rate;

public interface RateSensitive {
    void acceptRate(Rate rate);
}
