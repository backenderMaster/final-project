package ru.otus.calculator.counters;

import ru.otus.model.finance.Execution;

public interface Counter {
    void acceptExecution(Execution execution);
    long getValue();
}
