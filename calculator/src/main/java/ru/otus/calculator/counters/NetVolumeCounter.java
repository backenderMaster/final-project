package ru.otus.calculator.counters;

import ru.otus.model.finance.Currency;
import ru.otus.model.finance.Execution;
import ru.otus.model.finance.Rate;
import java.util.Objects;
import java.util.concurrent.atomic.DoubleAdder;

public class NetVolumeCounter implements Counter, RateSensitive {
    private final String instrumentId;
    private final Currency targetCurrency;
    private final DoubleAdder value;
    private volatile double rate;

    public NetVolumeCounter(String instrumentId, Currency targetCurrency, double rate) {
        this.instrumentId = instrumentId;
        this.targetCurrency = targetCurrency;
        this.rate = rate;
        this.value = new DoubleAdder();
    }

    @Override
    public void acceptExecution(Execution execution) {
        if (Objects.equals(instrumentId, execution.getInstrumentId())) {
            double delta = execution.getPrice() * execution.getQuantity() * rate;
            value.add(delta);
        }
    }

    @Override
    public long getValue() {
        return value.longValue();
    }

    @Override
    public void acceptRate(Rate rate) {
        if (rate.getTargetCurrency() == targetCurrency) {
            this.rate = rate.getValue();
        }
    }
}
