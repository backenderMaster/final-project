package ru.otus.calculator.snapshot;

import reactor.core.publisher.Flux;
import ru.otus.calculator.repositories.SnapshotRepository;
import ru.otus.calculator.workflows.ExecutionWorkflow;
import java.time.Duration;
import java.time.LocalDateTime;
import org.slf4j.Logger;
import ru.otus.model.metric.Snapshot;

public class SnapshotPersister {
    private final ExecutionWorkflow workflow;
    private final SnapshotRepository repository;
    private final Logger logger;
    private final Duration snapshotPeriod;

    public SnapshotPersister(
        ExecutionWorkflow workflow,
        SnapshotRepository repository,
        Logger logger,
        Duration snapshotPeriod
    ) {
        this.workflow = workflow;
        this.repository = repository;
        this.logger = logger;
        this.snapshotPeriod = snapshotPeriod;
    }

    public void startPersistingSnapshots() {
        Flux.interval(snapshotPeriod)
            .map(item -> new Snapshot(LocalDateTime.now(), workflow.getCountedMetrics()))
            .flatMap(repository::saveSnapshot)
            .doOnNext(snapshot -> logger.debug("Snapshot saved"))
            .subscribe();
    }
}
