package ru.otus.calculator.configuration;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.WebClient;
import ru.otus.calculator.counters.Counter;
import ru.otus.calculator.counters.RateSensitive;
import ru.otus.model.metric.Metric;
import ru.otus.calculator.workflows.ExecutionWorkflow;
import org.slf4j.Logger;
import ru.otus.calculator.workflows.RateWorkflow;
import java.util.Collection;
import java.util.Map;

@Configuration
public class WorkflowsConfig {
    @Bean
    public ExecutionWorkflow executionWorkflow(
        WebClient messagesGeneratorClient,
        @Value("${generator.executions.endpoint}") String executionsEndpoint,
        Map<Metric, Counter> metricsToCounters,
        Logger logger
    ) {
        return new ExecutionWorkflow(messagesGeneratorClient, executionsEndpoint, metricsToCounters, logger);
    }

    @Bean
    public WebClient webClient(@Value("${generator.host}") String host, @Value("${generator.port}") int port) {
        return WebClient.create("http://" + host + ":" + port + "/");
    }

    @Bean
    public Logger logger() {
        return LoggerFactory.getLogger("Logger");
    }

    @Bean
    public RateWorkflow rateWorkflow(
        WebClient webClient,
        Collection<RateSensitive> rateSensitives,
        @Value("${generator.rates.endpoint}") String ratesEndpoint,
        Logger logger
    ) {
        return new RateWorkflow(webClient, rateSensitives, ratesEndpoint, logger);
    }
}