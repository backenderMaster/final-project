package ru.otus.calculator.configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.otus.calculator.counters.Counter;
import ru.otus.calculator.counters.RateSensitive;
import ru.otus.model.metric.Metric;
import java.io.File;
import java.io.IOException;
import java.util.*;
import ru.otus.model.finance.Currency;

@Configuration
public class CountersConfig {
    @Bean
    public Collection<Metric> metrics(@Value("${metrics.file.path}") String filepath) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        return Arrays.asList(objectMapper.readValue(new File(filepath), Metric[].class));
    }

    @Bean
    public Map<Metric, Counter> metricsToCounters(Collection<Metric> metrics, CounterFactory counterFactory) {
        Map<Metric, Counter> map = new HashMap<>();
        for(Metric metric: metrics) {
            map.put(metric, counterFactory.createCounter(metric));
        }
        return map;
    }

    @Bean
    public CounterFactory counterFactory(@Autowired ApplicationContext context) {
        Map<Currency, Double> map = new HashMap<>();
        map.put(Currency.USD, context.getEnvironment().getProperty("default.rate.usd", Double.class));
        map.put(Currency.EUR, context.getEnvironment().getProperty("default.rate.eur", Double.class));
        return new CounterFactory(map);
    }

    @Bean
    public Collection<RateSensitive> rateSensitives(Map<Metric, Counter> metricToCounters) {
        Collection<RateSensitive> rateSensitives = new ArrayList<>();
        for(Map.Entry<Metric, Counter> entry: metricToCounters.entrySet()) {
            Counter counter = entry.getValue();
            if (counter instanceof RateSensitive) {
                RateSensitive rateSensitive = (RateSensitive) counter;
                rateSensitives.add(rateSensitive);
            }
        }
        return rateSensitives;
    }
}
