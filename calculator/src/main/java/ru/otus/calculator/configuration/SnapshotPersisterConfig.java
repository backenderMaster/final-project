package ru.otus.calculator.configuration;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.otus.calculator.repositories.SnapshotRepository;
import ru.otus.calculator.snapshot.SnapshotPersister;
import ru.otus.calculator.workflows.ExecutionWorkflow;
import java.time.Duration;

@Configuration
public class SnapshotPersisterConfig {
    @Bean
    public SnapshotPersister snapshotPersister(
        ExecutionWorkflow workflow,
        @Autowired SnapshotRepository repository,
        @Value("${snapshot.period.milliseconds}") int snapshotPeriodMillis,
        Logger logger
    ) {
        return new SnapshotPersister(workflow, repository, logger, Duration.ofMillis(snapshotPeriodMillis));
    }
}
