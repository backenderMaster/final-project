package ru.otus.calculator.configuration;

import ru.otus.calculator.counters.ActiveOrdersCounter;
import ru.otus.calculator.counters.Counter;
import ru.otus.calculator.counters.NetQuantityCounter;
import ru.otus.calculator.counters.NetVolumeCounter;
import ru.otus.model.metric.Metric;
import ru.otus.model.metric.MetricType;
import ru.otus.model.finance.Currency;
import java.util.Map;

public class CounterFactory {
    private final Map<Currency, Double> defaultRates;

    public CounterFactory(Map<Currency, Double> defaultRates) {
        this.defaultRates = defaultRates;
    }

    public Counter createCounter(Metric metric) {
        if (metric.getType() == MetricType.ActiveOrders) {
            return new ActiveOrdersCounter(metric.getInstrumentId());
        }

        if (metric.getType() == MetricType.NetQuantity) {
            return new NetQuantityCounter(metric.getInstrumentId());
        }

        if (metric.getType() == MetricType.NetVolume) {
            return new NetVolumeCounter(metric.getInstrumentId(), metric.getTargetCurrency(), getInitialRate(metric));
        }
        throw new RuntimeException("Could not create counter for metric type: " + metric.getType());
    }

    private double getInitialRate(Metric metric) {
        double initialRate = 1;
        if(metric.getExecutionCurrency() != metric.getTargetCurrency()) {
            initialRate = defaultRates.get(metric.getTargetCurrency());
        }
        return initialRate;
    }
}
