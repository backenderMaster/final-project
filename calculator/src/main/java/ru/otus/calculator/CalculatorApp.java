package ru.otus.calculator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.data.mongodb.repository.config.EnableReactiveMongoRepositories;
import ru.otus.calculator.snapshot.SnapshotPersister;
import ru.otus.calculator.workflows.ExecutionWorkflow;
import ru.otus.calculator.workflows.RateWorkflow;

@SpringBootApplication
@EnableReactiveMongoRepositories
public class CalculatorApp {
    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(CalculatorApp.class);

        RateWorkflow rateWorkflow = context.getBean(RateWorkflow.class);
        rateWorkflow.start();

        ExecutionWorkflow executionWorkflow = context.getBean(ExecutionWorkflow.class);
        executionWorkflow.start();

        SnapshotPersister snapshotPersister = context.getBean(SnapshotPersister.class);
        snapshotPersister.startPersistingSnapshots();
    }
}