package ru.otus.calculator.workflows;

import org.slf4j.Logger;
import org.springframework.web.reactive.function.client.WebClient;
import ru.otus.calculator.counters.Counter;
import ru.otus.model.metric.Metric;
import ru.otus.model.finance.Execution;
import java.util.Collection;
import java.util.Map;
import java.util.stream.Collectors;

public class ExecutionWorkflow {
    private final WebClient webClient;
    private final Map<Metric, Counter> metricsToCounters;
    private final String executionsEndpoint;
    private final Logger logger;

    public ExecutionWorkflow(
        WebClient webClient,
        String executionsEndpoint,
        Map<Metric, Counter> metricsToCounters,
        Logger logger
    ) {
        this.webClient = webClient;
        this.executionsEndpoint = executionsEndpoint;
        this.metricsToCounters = metricsToCounters;
        this.logger = logger;
    }

    public void start() {
        webClient.get()
            .uri(executionsEndpoint)
            .retrieve()
            .bodyToFlux(Execution.class)
            .doOnNext(execution -> logger.debug("Received execution: " + execution))
            .doOnNext(this::propagateExecution)
            .subscribe();
    }

    private void propagateExecution(Execution execution) {
        for (Map.Entry<Metric, Counter> entry : metricsToCounters.entrySet()) {
            entry.getValue().acceptExecution(execution);
        }
    }

    public Collection<Metric> getCountedMetrics() {
        return metricsToCounters.entrySet()
            .stream()
            .map(entry -> {
                Metric metric = entry.getKey();
                Counter counter = entry.getValue();
                metric.setCurrentValue(counter.getValue());
                return metric;
            })
            .collect(Collectors.toList());
    }
}