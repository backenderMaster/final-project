package ru.otus.calculator.workflows;

import org.slf4j.Logger;
import org.springframework.web.reactive.function.client.WebClient;
import ru.otus.calculator.counters.RateSensitive;
import ru.otus.model.finance.Rate;
import java.util.Collection;

public class RateWorkflow {
    private final WebClient webClient;
    private final Collection<RateSensitive> rateSensitiveCounters;
    private final String ratesEndpoint;
    private final Logger logger;

    public RateWorkflow(
        WebClient webClient,
        Collection<RateSensitive> rateSensitiveCounters,
        String ratesEndpoint,
        Logger logger
    ) {
        this.webClient = webClient;
        this.rateSensitiveCounters = rateSensitiveCounters;
        this.ratesEndpoint = ratesEndpoint;
        this.logger = logger;
    }

    public void start() {
        webClient.get()
            .uri(ratesEndpoint)
            .retrieve()
            .bodyToFlux(Rate.class)
            .doOnNext(rate -> logger.debug("Received rate: " + rate))
            .doOnNext(this::propagateRate)
            .subscribe();
    }

    private void propagateRate(Rate rate) {
        for(RateSensitive rateSensitiveCounter: rateSensitiveCounters) {
            rateSensitiveCounter.acceptRate(rate);
        }
    }
}