package ru.otus.calculator.repositories;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import ru.otus.model.metric.Snapshot;
import java.time.LocalDateTime;
import java.util.List;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

@DataMongoTest
class SnapshotRepositoryTest {
    @Autowired
    private ReactiveMongoTemplate mongoTemplate;
    @Value("${snapshots.collection.name}")
    private String collectionName;

    @BeforeEach
    public void clearCollection() {
        mongoTemplate.remove(Snapshot.class).all().block();
    }

    @Test
    public void repositoryMethodMustReturnTheLatestSnapshot() {
        int nanoseconds = 123_000_000;
        LocalDateTime timestamp = LocalDateTime.now().withNano(nanoseconds);
        int offsetMinutes = 1;

        mongoTemplate.insert(
            new Snapshot(timestamp.minusMinutes(offsetMinutes).minusMinutes(offsetMinutes), List.of())
        ).block();
        mongoTemplate.insert(new Snapshot(timestamp.minusMinutes(offsetMinutes), List.of())).block();
        mongoTemplate.insert(new Snapshot(timestamp, List.of())).block();

        Snapshot snapshot = new SnapshotRepository(mongoTemplate, collectionName).findLatestSnapshot().block();
        assertEquals(timestamp, snapshot.getTimestamp());
    }

    @Test
    public void findingLatestSnapshotMustNotLeadToExceptionsWhenThereAreNoSnapshots() {
        Snapshot snapshot = new SnapshotRepository(mongoTemplate, collectionName).findLatestSnapshot().block();
        assertNull(snapshot);
    }
}