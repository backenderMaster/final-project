package ru.otus.calculator.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import reactor.core.publisher.Mono;
import ru.otus.calculator.repositories.SnapshotRepository;
import ru.otus.model.metric.Metric;
import ru.otus.model.metric.MetricType;
import ru.otus.model.metric.Snapshot;
import ru.otus.model.finance.Desk;
import java.time.LocalDateTime;
import java.util.List;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static ru.otus.model.finance.Currency.RUB;
import static ru.otus.model.finance.Currency.USD;

@WebMvcTest(SnapshotController.class)
class SnapshotControllerTest {
    @Autowired
    private MockMvc mvc;
    @MockBean
    private SnapshotRepository repository;

    @Test
    public void allTheReturnedMetricsMustCorrespondToSpecifiedDesk() throws Exception {
        Snapshot snapshot = new Snapshot(
            LocalDateTime.now(),
            List.of(
                createMetric(Desk.DESK_ONE),
                createMetric(Desk.DESK_ONE),
                createMetric(Desk.DESK_TWO),
                createMetric(Desk.DESK_TWO)
            )
        );
        given(repository.findLatestSnapshot()).willReturn(Mono.just(snapshot));
        mvc.perform(get("/snapshot").header("desk", Desk.DESK_ONE.toString()))
            .andExpect(jsonPath("$.metrics.[0].desk").value(Desk.DESK_ONE.toString()))
            .andExpect(jsonPath("$.metrics.[1].desk").value(Desk.DESK_ONE.toString()));
    }

    private Metric createMetric(Desk desk) {
        String name = "metric-name";
        int currentValue = 0;
        int borderValue = 1;
        String instrumentId = "instrumentId";
        return new Metric(name, currentValue, borderValue, instrumentId, MetricType.ActiveOrders, RUB, USD, desk);
    }

    @Test
    public void controllerMustReturnCertainNumberOfMetrics() throws Exception {
        Snapshot snapshot = new Snapshot(
            LocalDateTime.now(),
            List.of(
                createMetric(Desk.DESK_ONE),
                createMetric(Desk.DESK_ONE),
                createMetric(Desk.DESK_TWO),
                createMetric(Desk.DESK_TWO)
            )
        );
        given(repository.findLatestSnapshot()).willReturn(Mono.just(snapshot));
        mvc.perform(get("/snapshot").header("desk", Desk.DESK_ONE.toString()))
            .andExpect(jsonPath("$.metrics.[0]").exists())
            .andExpect(jsonPath("$.metrics.[1]").exists())
            .andExpect(jsonPath("$.metrics.[2]").doesNotExist());
    }
}