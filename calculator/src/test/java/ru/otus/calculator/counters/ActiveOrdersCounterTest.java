package ru.otus.calculator.counters;

import org.junit.jupiter.api.Test;
import ru.otus.model.finance.Currency;
import ru.otus.model.finance.Execution;
import ru.otus.model.finance.ExecutionStatus;
import static org.junit.jupiter.api.Assertions.*;

class ActiveOrdersCounterTest {
    private final String instrumentId = "instrumentId";

    private Execution createExecution(ExecutionStatus status) {
        int orderId = 1;
        int price = 0;
        int quantity = 1;
        return new Execution(orderId, instrumentId, price, quantity, status, Currency.RUB);
    }

    @Test
    public void counterValueMustIncreaseByOneWhenExecutionWithInitialStatusComes() {
        ActiveOrdersCounter counter = new ActiveOrdersCounter(instrumentId);
        Execution execution = createExecution(ExecutionStatus.Initial);
        counter.acceptExecution(execution);
        int expectedValue = 1;
        assertEquals(expectedValue, counter.getValue());
    }

    @Test
    public void counterValueMustDecreaseByOneWhenExecutionWithTerminalStatusComes() {
        ActiveOrdersCounter counter = new ActiveOrdersCounter(instrumentId);

        Execution initialExecution = createExecution(ExecutionStatus.Initial);
        Execution terminalExecution = createExecution(ExecutionStatus.Terminal);

        counter.acceptExecution(initialExecution);
        counter.acceptExecution(terminalExecution);

        int expectedValue = 0;
        assertEquals(expectedValue, counter.getValue());
    }

    @Test
    public void counterMustBeInitializedWithZeroValue() {
        ActiveOrdersCounter counter = new ActiveOrdersCounter(instrumentId);
        int expectedValue = 0;
        assertEquals(expectedValue, counter.getValue());
    }
}