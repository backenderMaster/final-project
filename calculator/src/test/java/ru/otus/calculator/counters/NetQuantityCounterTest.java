package ru.otus.calculator.counters;

import org.junit.jupiter.api.Test;
import ru.otus.model.finance.Currency;
import ru.otus.model.finance.Execution;
import ru.otus.model.finance.ExecutionStatus;
import static org.junit.jupiter.api.Assertions.*;

class NetQuantityCounterTest {
    private final String instrumentId = "instrumentId";

    @Test
    public void counterMustBeInitializedWithZeroValue() {
        NetQuantityCounter counter = new NetQuantityCounter(instrumentId);
        int expectedValue = 0;
        assertEquals(expectedValue, counter.getValue());
    }

    @Test
    public void counterValueMustIncreaseByExecutedQuantityWhenExecutionComes() {
        NetQuantityCounter counter = new NetQuantityCounter(instrumentId);
        int quantity = 100;
        int orderId = 1;
        int price = 0;
        Execution execution = new Execution(orderId, instrumentId, price, quantity, ExecutionStatus.Initial, Currency.RUB);
        counter.acceptExecution(execution);
        assertEquals(quantity, counter.getValue());
    }
}