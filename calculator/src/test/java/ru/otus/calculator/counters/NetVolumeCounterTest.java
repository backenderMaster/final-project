package ru.otus.calculator.counters;

import org.junit.jupiter.api.Test;
import ru.otus.model.finance.Currency;
import ru.otus.model.finance.Execution;
import ru.otus.model.finance.ExecutionStatus;
import ru.otus.model.finance.Rate;
import static org.junit.jupiter.api.Assertions.*;

class NetVolumeCounterTest {
    private final String instrumentId = "instrumentId";
    private final Currency targetCurrency = Currency.USD;

    @Test
    public void counterMustBeInitializedWithZeroValue() {
        double initialRate = 1;
        NetVolumeCounter counter = new NetVolumeCounter(instrumentId, targetCurrency, initialRate);
        int expectedValue = 0;
        assertEquals(expectedValue, counter.getValue());
    }

    @Test
    public void counterMustConsiderRate() {
        double initialRate = 1;
        NetVolumeCounter counter = new NetVolumeCounter(instrumentId, targetCurrency, initialRate);

        int quantity = 1000;
        double price = 1.5;
        int orderId = 1;
        Execution execution = new Execution(orderId, instrumentId, price, quantity, ExecutionStatus.Initial, Currency.RUB);

        double rubToUsdRatio = 0.015;
        Rate rate = new Rate(Currency.RUB, Currency.USD, rubToUsdRatio);

        counter.acceptRate(rate);
        counter.acceptExecution(execution);

        int expectedValue = 22;
        assertEquals(expectedValue, counter.getValue());
    }
}