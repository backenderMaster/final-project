package ru.otus.generator.suppliers;

import org.junit.jupiter.api.Test;
import ru.otus.model.finance.Execution;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import static org.junit.jupiter.api.Assertions.*;

class MultiInstrumentExecutionSupplierTest {
    @Test
    public void multiInstrumentSupplierMustPollDaughterSuppliersOneByOne() {
        Collection<String> initialInstrumentIds = List.of("inst1", "inst2", "inst3", "inst4", "inst5", "inst6", "inst7");
        List<SingleInstrumentExecutionSupplier> singleInstrumentExecutionSuppliers = initialInstrumentIds.stream()
            .map(this::createSingleInstrumentSupplier)
            .collect(Collectors.toList());
        MultiInstrumentExecutionSupplier multiInstrumentExecutionSupplier = new MultiInstrumentExecutionSupplier(
            singleInstrumentExecutionSuppliers
        );
        Collection<String> derivedInstrumentIds = initialInstrumentIds.stream()
            .map(id -> multiInstrumentExecutionSupplier.get())
            .map(Execution::getInstrumentId)
            .collect(Collectors.toList());

        assertTrue(initialInstrumentIds.containsAll(derivedInstrumentIds));
        assertEquals(initialInstrumentIds.size(), derivedInstrumentIds.size());
    }

    private SingleInstrumentExecutionSupplier createSingleInstrumentSupplier(String instrumentId) {
        int fillingRate = 1;
        int volume = 10;
        int price = 0;
        int orderId = 0;
        return new SingleInstrumentExecutionSupplier(fillingRate, volume, instrumentId, price, orderId);
    }
}