package ru.otus.generator.suppliers;

import org.junit.jupiter.api.Test;
import ru.otus.model.finance.Execution;
import ru.otus.model.finance.ExecutionStatus;
import java.util.function.Supplier;
import static org.junit.jupiter.api.Assertions.assertEquals;

class SingleInstrumentExecutionSupplierTest {

    private final String instrumentId = "instrumentId";
    private final double price = 1.5;
    private final int fillingRate = 1;
    private final int orderId = 0;

    @Test
    public void firstExecutionMustHaveInitialStatus() {
        int volume = 2;
        Supplier<Execution> supplier = new SingleInstrumentExecutionSupplier(fillingRate, volume, instrumentId, price, orderId);
        Execution execution = supplier.get();
        assertEquals(ExecutionStatus.Initial, execution.getStatus());
    }

    @Test
    public void lastExecutionMustHaveTerminalStatus() {
        int volume = 2;
        Supplier<Execution> supplier = new SingleInstrumentExecutionSupplier(fillingRate, volume, instrumentId, price, orderId);
        supplier.get();
        Execution execution = supplier.get();
        assertEquals(ExecutionStatus.Terminal, execution.getStatus());
    }

    @Test
    public void orderIdMustBeIncreasedByOneAfterLastExecution() {
        int volume = 2;
        Supplier<Execution> supplier = new SingleInstrumentExecutionSupplier(fillingRate, volume, instrumentId, price, orderId);
        supplier.get();
        supplier.get();
        Execution execution = supplier.get();
        int expectedOrderId = orderId + 1;
        assertEquals(expectedOrderId, execution.getOrderId());
    }

    @Test
    public void executedVolumeMustBeSetToZeroAfterLastExecution() {
        int volume = 2;
        Supplier<Execution> supplier = new SingleInstrumentExecutionSupplier(fillingRate, volume, instrumentId, price, orderId);
        supplier.get();
        supplier.get();
        Execution execution = supplier.get();
        assertEquals(fillingRate, execution.getQuantity());
    }

    @Test
    public void intermediateExecutionsMustHaveIntermediateStatus() {
        int volume = 3;
        Supplier<Execution> supplier = new SingleInstrumentExecutionSupplier(fillingRate, volume, instrumentId, price, orderId);
        supplier.get();
        Execution execution = supplier.get();
        assertEquals(ExecutionStatus.Intermediate, execution.getStatus());
    }
}