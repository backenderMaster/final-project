package ru.otus.generator.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import ru.otus.model.finance.Execution;
import ru.otus.model.finance.Rate;

@RestController
public class MarketDataController {
    private final Flux<Execution> executionsFlux;
    private final Flux<Rate> ratesFlux;

    public MarketDataController(@Autowired Flux<Execution> executionsFlux, @Autowired Flux<Rate> ratesFlux) {
        this.executionsFlux = executionsFlux;
        this.ratesFlux = ratesFlux;
    }

    @GetMapping(path = "/executions", produces = MediaType.APPLICATION_NDJSON_VALUE)
    public Flux<Execution> getOrders() {
        return executionsFlux;
    }

    @GetMapping(path = "/rates", produces = MediaType.APPLICATION_NDJSON_VALUE)
    public Flux<Rate> getRates() {
        return ratesFlux;
    }
}
