package ru.otus.generator.controllers;

import ru.otus.model.finance.Currency;
import ru.otus.model.finance.Rate;

import java.util.function.Supplier;

public class RateGenerator implements Supplier<Rate> {
    @Override
    public Rate get() {
        return new Rate(Currency.RUB, Currency.USD, 0.013);
    }
}
