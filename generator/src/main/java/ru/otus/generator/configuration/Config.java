package ru.otus.generator.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import reactor.core.publisher.Flux;
import ru.otus.generator.suppliers.MultiInstrumentExecutionSupplier;
import ru.otus.generator.suppliers.SingleInstrumentExecutionSupplier;
import ru.otus.generator.suppliers.RateSupplier;
import ru.otus.model.finance.Currency;
import ru.otus.model.finance.Execution;
import ru.otus.model.finance.Rate;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Configuration
public class Config {
    @Bean
    public Collection<SingleInstrumentExecutionSupplier> executionSuppliers(
        @Value("${instrument.identifiers}") Collection<String> instrumentIds,
        ExecutionSupplierFactory executionSupplierFactory
    ) {
        List<SingleInstrumentExecutionSupplier> suppliers = new ArrayList<>();
        int suppliersPerInstrument = 10;
        int step = 1_000_000;
        instrumentIds.forEach(instrumentId -> {
            for(int i = 0; i < suppliersPerInstrument; i++) {
                int orderId = i * step;
                suppliers.add(executionSupplierFactory.getExecutionSupplier(instrumentId, orderId));
            }
        });
        return suppliers;
    }

    @Bean
    public ExecutionSupplierFactory executionSupplierFactory() {
        List<Integer> fillingRates = List.of(250, 500, 1000);
        List<Integer> volumes = List.of(10_000, 12_000, 15_000, 17_000, 20_000, 22_000, 25_000, 27_000, 30_000);
        List<Double> prices = List.of(131.1, 2033.6, 1500.8, 65.3, 156.7, 1344.4, 522.5, 372.9);
        return new ExecutionSupplierFactory(fillingRates, prices, volumes);
    }

    @Bean
    public Flux<Execution> executionFlux(
        Collection<SingleInstrumentExecutionSupplier> singleInstrumentExecutionSuppliers,
        @Value("${executions.delay.millis}") int elementsDelayMillis
    ) {
        MultiInstrumentExecutionSupplier supplier = new MultiInstrumentExecutionSupplier(
            singleInstrumentExecutionSuppliers
        );
        Stream<Execution> stream = Stream.generate(supplier);
        return Flux.fromStream(stream).delayElements(Duration.ofMillis(elementsDelayMillis));
    }

    @Bean
    public RateSupplier rateSupplier() {
        List<Rate> rates = List.of(
            new Rate(Currency.RUB, Currency.EUR, 0.0125),
            new Rate(Currency.RUB, Currency.EUR, 0.0127),
            new Rate(Currency.RUB, Currency.EUR, 0.0129),
            new Rate(Currency.RUB, Currency.EUR, 0.0131),
            new Rate(Currency.RUB, Currency.EUR, 0.0133),
            new Rate(Currency.RUB, Currency.USD, 0.0135),
            new Rate(Currency.RUB, Currency.USD, 0.0137),
            new Rate(Currency.RUB, Currency.USD, 0.0139),
            new Rate(Currency.RUB, Currency.USD, 0.0141),
            new Rate(Currency.RUB, Currency.USD, 0.0143)
        );
        return new RateSupplier(rates);
    }

    @Bean
    public Flux<Rate> rateFlux(
        RateSupplier rateSupplier,
        @Value("${rates.delay.millis}") int delayElementsMillis
    ) {
        Stream<Rate> stream = Stream.generate(rateSupplier);
        return Flux.fromStream(stream).delayElements(Duration.ofMillis(delayElementsMillis)).share();
    }
}