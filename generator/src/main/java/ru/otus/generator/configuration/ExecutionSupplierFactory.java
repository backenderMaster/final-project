package ru.otus.generator.configuration;

import ru.otus.generator.suppliers.SingleInstrumentExecutionSupplier;
import java.util.List;
import java.util.Random;

public class ExecutionSupplierFactory {
    private final List<Integer> fillingRates;
    private final List<Double> prices;
    private final List<Integer> volumes;

    public ExecutionSupplierFactory(List<Integer> fillingRates, List<Double> prices, List<Integer> volumes) {
        this.fillingRates = fillingRates;
        this.prices = prices;
        this.volumes = volumes;
    }

    private int getRandomFillingRate() {
        int index = new Random().nextInt(fillingRates.size());
        return fillingRates.get(index);
    }

    private double getRandomPrice() {
        int index = new Random().nextInt(prices.size());
        return prices.get(index);
    }

    private int getRandomVolume() {
        int index = new Random().nextInt(volumes.size());
        return volumes.get(index);
    }

    public SingleInstrumentExecutionSupplier getExecutionSupplier(String instrumentId, long orderId) {
        return new SingleInstrumentExecutionSupplier(
            getRandomFillingRate(),
            getRandomVolume(),
            instrumentId,
            getRandomPrice(),
            orderId
        );
    }
}