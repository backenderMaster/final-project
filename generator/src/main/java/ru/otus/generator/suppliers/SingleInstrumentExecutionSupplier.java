package ru.otus.generator.suppliers;

import ru.otus.model.finance.Currency;
import ru.otus.model.finance.Execution;
import ru.otus.model.finance.ExecutionStatus;
import java.util.function.Supplier;

public class SingleInstrumentExecutionSupplier implements Supplier<Execution> {
    private final int fillingRate;
    private final int desiredVolume;
    private final String instrumentId;
    private final double price;
    private long orderId;
    private int executedVolume;

    public SingleInstrumentExecutionSupplier(
        int fillingRate,
        int volume,
        String instrumentId,
        double price,
        long orderId
    ) {
        this.fillingRate = fillingRate;
        this.desiredVolume = volume;
        this.instrumentId = instrumentId;
        this.price = price;
        this.orderId = orderId;
        executedVolume = 0;
    }

    @Override
    public Execution get() {
        if (executedVolume == 0) {
            executedVolume += fillingRate;
            return new Execution(orderId, instrumentId, price, fillingRate, ExecutionStatus.Initial, Currency.RUB);
        }
        if (executedVolume == desiredVolume - fillingRate) {
            orderId += 1;
            executedVolume = 0;
            return new Execution(orderId, instrumentId, price, desiredVolume, ExecutionStatus.Terminal, Currency.RUB);
        }
        executedVolume += fillingRate;
        return new Execution(orderId, instrumentId, price, executedVolume, ExecutionStatus.Intermediate, Currency.RUB);
    }
}
