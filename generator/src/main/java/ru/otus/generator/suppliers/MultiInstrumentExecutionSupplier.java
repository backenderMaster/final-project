package ru.otus.generator.suppliers;

import ru.otus.model.finance.Execution;

import java.util.Collection;
import java.util.List;
import java.util.function.Supplier;

public class MultiInstrumentExecutionSupplier implements Supplier<Execution> {
    private final List<SingleInstrumentExecutionSupplier> suppliers;
    private long counter = 0;

    public MultiInstrumentExecutionSupplier(Collection<SingleInstrumentExecutionSupplier> suppliers) {
        this.suppliers = List.copyOf(suppliers);
    }

    @Override
    public Execution get() {
        counter+=1;
        int index = (int) (counter % suppliers.size());
        return suppliers.get(index).get();
    }
}
