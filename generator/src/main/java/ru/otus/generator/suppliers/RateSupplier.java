package ru.otus.generator.suppliers;

import ru.otus.model.finance.Rate;

import java.util.List;
import java.util.Random;
import java.util.function.Supplier;

public class RateSupplier implements Supplier<Rate> {
    private final List<Rate> rates;
    private final Random random;

    public RateSupplier(List<Rate> rates) {
        this.rates = rates;
        random = new Random();
    }

    @Override
    public Rate get() {
        int index = random.nextInt(rates.size());
        return rates.get(index);
    }
}
