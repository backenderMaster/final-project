package ru.otus.model.metric;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.otus.model.finance.Currency;
import ru.otus.model.finance.Desk;

@NoArgsConstructor
@Data
@AllArgsConstructor
public class Metric {
    private String name;
    private long currentValue;
    private long borderValue;
    private String instrumentId;
    private MetricType type;
    private Currency executionCurrency;
    private Currency targetCurrency;
    private Desk desk;
}