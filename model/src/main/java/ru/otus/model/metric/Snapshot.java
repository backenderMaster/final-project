package ru.otus.model.metric;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.time.LocalDateTime;
import java.util.Collection;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Snapshot {
    private LocalDateTime timestamp;
    private Collection<Metric> metrics;
}
