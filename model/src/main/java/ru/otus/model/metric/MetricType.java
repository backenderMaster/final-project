package ru.otus.model.metric;

public enum MetricType {
    NetVolume, NetQuantity, ActiveOrders
}
