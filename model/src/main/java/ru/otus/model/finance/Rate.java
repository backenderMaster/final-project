package ru.otus.model.finance;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Rate {
    private Currency baseCurrency;
    private Currency targetCurrency;
    private double value;
}