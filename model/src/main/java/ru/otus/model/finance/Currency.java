package ru.otus.model.finance;

public enum Currency {
    USD, EUR, RUB
}
