package ru.otus.model.finance;

public enum ExecutionStatus {
    Initial, Intermediate, Terminal
}
