package ru.otus.model.finance;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Execution {
    private long orderId;
    private String instrumentId;
    private double price;
    private int quantity;
    private ExecutionStatus status;
    private Currency currency;
}
